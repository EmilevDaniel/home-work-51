import React from 'react';
import Roll from "./Roll";
import './Rolls.css'

const Rolls = props => {
    return (
        <div className="Rolls">
            <button onClick={props.rollNumber}>New numbers</button>
            <div className={"numbersHolder"}>
                <Roll number={props.roll[0].roll1}/>
                <Roll number={props.roll[0].roll2}/>
                <Roll number={props.roll[0].roll3}/>
                <Roll number={props.roll[0].roll4}/>
                <Roll number={props.roll[0].roll5}/>
            </div>


        </div>
    );
};


export default Rolls;