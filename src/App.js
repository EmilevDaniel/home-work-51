import Rolls from "./Roll/Rolls";
import "./App.css"
import {Component} from "react";


class App extends Component {
    state = {
        roll: [
            {roll1: ''},
            {roll2: ''},
            {roll3: ''},
            {roll4: ''},
            {roll5: ''},
        ]
    };


    rollNumber = () => {
        const newNumbers = [];
        let number;
        let unic;
        while (newNumbers.length < 5) {
            do {
                unic = true;
                number = Math.floor(Math.random() * 36) + 1;
                for (let i = 0; i < newNumbers.length; i++) {
                    if (number === newNumbers[i]) {
                        unic = false;
                        break;
                    }
                }
            } while (!unic);
            newNumbers.push(number);
        }
        const numbers = newNumbers.sort((a, b) => a > b ? 1 : -1);


        this.setState({
            roll: this.state.roll.map((p, i) => {
                return {
                    ...p,
                    roll1: numbers[0],
                    roll2: numbers[1],
                    roll3: numbers[2],
                    roll4: numbers[3],
                    roll5: numbers[4],
                }
            })
        })
    };

    render() {
        return (
            <div className={'App'}>
                <Rolls roll={this.state.roll} rollNumber={this.rollNumber}/>

            </div>
        );
    }
}

export default App;
